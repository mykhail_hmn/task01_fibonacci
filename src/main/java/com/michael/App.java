package com.michael;

import com.michael.service.impl.FibonacciImpl;
import com.michael.service.impl.IntervalImpl;

import java.util.Scanner;

/**
 * Main class where you starting your program.
 * Follow instructions of SCreen.
 */
public class App {
    private static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("=========Hello=========");
        System.out.println("Choose the one:");
        System.out.println("1: Task2.1: Interval");
        System.out.println("2: Task2.2: Fibonacci");
        while (SC.hasNext()) {
            selectTask(SC.nextInt());
        }
    }

    private static void selectTask(int choice) {
        IntervalImpl interval = new IntervalImpl();
        FibonacciImpl fibonacci = new FibonacciImpl();
        switch (choice) {
            case 1:
                System.out.println("1.User enter the interval (for example: [1;100]);");
                System.out.println("2.Program prints odd numbers from" +
                                "start to the end of interval and even from end to start");

                System.out.println("3.Program prints the sum of odd and even numbers");
                System.out.println("0.Back");
                System.out.println("Your choice:");

                while (SC.hasNext()) {
                    int choiceInterval = SC.nextInt();
                    switch (choiceInterval) {
                        case 1:
                            System.out.println("Enter interval");
                            System.out.print("Enter start position:");
                            int start = SC.nextInt();
                            System.out.print("Enter start position:");
                            int end = SC.nextInt();
                            interval.buildList(start, end);
                            System.out.println("Your List" + interval.print());
                            break;
                        case 2:
                            System.out.print("Odd Numbers:" + interval.getOddNumbers());
                            System.out.println();
                            System.out.print("Even Numbers:" + interval.getEvenNumbers());
                            System.out.println();
                            break;
                        case 3:
                            System.out.print("Even Sum:" + interval.evenSum());
                            System.out.println();
                            System.out.print("Odd Sum:" + interval.oddSum());
                            break;
                        case 0:
                            return;
                        default:
                            System.err.println("Try again");
                            break;
                    }
                }
                break;

            case 2:
                System.out.println("1.Program build Fibonacci numbers, user can enter the size of set (N);");
                System.out.println("2.F1 will be the biggest odd number and F2 – the biggest even number");
                System.out.println("3.Print percentage of odd and even Fibonacci numbers");
                System.out.println("0.Back");
                while (SC.hasNext()) {
                    int choiceFibonacci = SC.nextInt();
                    switch (choiceFibonacci) {
                        case 1:
                            System.out.print("Enter size of Fibonacci line:");
                            int size = SC.nextInt();
                            fibonacci.buildFibonacci(size);
                            System.out.println(fibonacci.print());
                            break;
                        case 2:

                            System.out.println("F1:" + fibonacci.maxOdd());
                            System.out.println("F2:" + fibonacci.maxEven());
                            break;
                        case 3:
                            fibonacci.percentage();
                            System.out.println("Odd numbers percentage:" + fibonacci.showOddPerscentage());
                            System.out.println("Even numbers percentage:" + fibonacci.showEvenPerscentage());
                            break;
                        case 0:
                            return;
                        default:
                            System.err.println("Try again");
                            break;
                    }
                }
                break;


        }
    }
}
