package com.michael.service.impl;


import com.michael.service.interfaces.Fibonacci;

import java.util.*;
import java.util.function.Predicate;

/**
 * Service layer class implements the methods
 * for working with Fibonacci numbers.
 */
public class FibonacciImpl implements Fibonacci {

    private double persentageEven;
    private double persentageOdd;
    private List<Long> fibonacci = new ArrayList<>();
    Predicate<Long> evenFunc = (num) -> num % 2 == 0;
    Predicate<Long> oddFunc = evenFunc.negate();


    /**
     * Building arrayList with Fibonacci numbers
     *
     * @param size defines the size of List.
     */

    @Override
    public void buildFibonacci(int size) {
        long n0 = 1, n1 = 1, n2;
        fibonacci.add(n0);
        fibonacci.add(n1);
        try {
            for (int i = 0; i <= size - 3; i++) {
                n2 = n0 + n1;
                fibonacci.add(n2);
                n0 = n1;
                n1 = n2;
                if (n2 < 0) {
                    throw new IndexOutOfBoundsException("Numbers too big");
                }
            }
        } catch (Exception e) {
            System.err.println("Too large");
            return;
        }

    }

    /**
     * Printing numbers on your screen
     */
    @Override
    public List<Long> print() {
        return fibonacci;
    }

    /**
     * Finding max odd number from list
     *
     * @return OptionalLong F1.
     */
    @Override
    public OptionalLong maxOdd() {
        OptionalLong F1 = fibonacci.stream()
                .filter(oddFunc)
                .mapToLong((num) -> num).max();
        return F1;
    }

    /**
     * Finding max even number from list
     *
     * @return OptionalLong F2.
     */
    @Override
    public OptionalLong maxEven() {
        OptionalLong F2 = fibonacci.stream()
                .filter(evenFunc)
                .mapToLong((num) -> num).max();
        return F2;
    }

    /**
     * Counting percentage of odd and even numbers in fibonacci sequence
     *
     * @return double.
     */
    @Override
    public void percentage() {
        long size = fibonacci.stream()
                .count();
        long oddNumberCount = fibonacci.stream()
                .filter(oddFunc).count();
        long evenNumberCount = fibonacci.stream()
                .filter(evenFunc).count();

        persentageOdd = oddNumberCount * 100 / size;
        persentageEven = evenNumberCount * 100 / size;
    }

    @Override
    public double showOddPerscentage() {
        return persentageOdd;
    }

    @Override
    public double showEvenPerscentage() {
        return persentageEven;

    }

}
