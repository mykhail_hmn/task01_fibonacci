package com.michael.service.impl;

import com.michael.service.interfaces.Interval;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Service layer class implements the methods
 * for working with task Interval.
 */

public class IntervalImpl implements Interval {
    private List<Integer> integerList = new ArrayList<>();
    /**
     * verifies that a certain condition is met, if number in list is even.
     */
    Predicate<Integer> evenFunc = (num) -> num % 2 == 0;
    /**
     * verifies that a certain condition is met, if number in list is odd.
     */
    Predicate<Integer> oddFunc = evenFunc.negate();

    /**
     * Building interval
     *
     * @param start defines the start number of interval.
     * @param end   defines the end number of interval
     */

    @Override
    public List<Integer> buildList(int start, int end) {
        for (int i = start; i <= end; i++) {
            integerList.add(i);
        }
        return integerList;
    }

    /**
     * Printing interval on your screen
     */
    @Override
    public List<Integer> print() {
        return integerList;
    }

    /**
     * Building new List with odd numbers from our List with interval
     *
     * @return new List oddNumbers.
     */
    @Override
    public List<Integer> getOddNumbers() {
        List<Integer> oddNumbers =
                integerList.stream()
                        .filter(oddFunc)
                        .collect(Collectors.toList());
        return oddNumbers;
    }

    /**
     * Building new List with even numbers from our List with interval
     *
     * @return new List evenNumbers.
     */
    @Override
    public List<Integer> getEvenNumbers() {
        List<Integer> evenNumbers =
                integerList.stream()
                        .filter(evenFunc)
                        .sorted(Collections.reverseOrder())
                        .collect(Collectors.toList());
        return evenNumbers;
    }

    /**
     * Calculating sum of all odd numbers in interval
     *
     * @return sum.
     */
    @Override
    public int oddSum() {
        int sum = integerList.stream().filter(oddFunc).mapToInt((num) -> num).sum();
        return sum;
    }

    /**
     * Calculating sum of all even numbers in interval
     *
     * @return sum.
     */
    @Override
    public int evenSum() {
        int sum = integerList.stream().filter(evenFunc).mapToInt((num) -> num).sum();
        return sum;
    }

}



