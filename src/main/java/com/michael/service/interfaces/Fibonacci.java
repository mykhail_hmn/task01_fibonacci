package com.michael.service.interfaces;

import com.michael.service.impl.FibonacciImpl;

import java.util.List;
import java.util.OptionalLong;

/**
 * The interface of the service layer, describes a set of methods for working
 * with class objects {@link FibonacciImpl}.
 */
public interface Fibonacci {
    void buildFibonacci(int size);

    List<Long> print();

    OptionalLong maxOdd();

    OptionalLong maxEven();

    void percentage();

    double showOddPerscentage();

    double showEvenPerscentage();

}
