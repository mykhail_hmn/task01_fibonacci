package com.michael.service.interfaces;

import com.michael.service.impl.IntervalImpl;

import java.util.List;

/**
 * The interface of the service layer, describes a set of methods for working
 * with class objects {@link IntervalImpl}.
 */
public interface Interval {

    List<Integer> buildList(int start, int end);

    List<Integer> print();

    List<Integer> getOddNumbers();

    List<Integer> getEvenNumbers();

    int oddSum();

    int evenSum();
}
